/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"


json_t *
Context_process_vui_command(Context_t *ctx, const enum Command cmd, const json_t *arg)
{
  json_t *res = NULL;

  switch (cmd) {
  case VUI_CONNECT:
    res = vui_connect(ctx);
    break;

  case VUI_DELETE_SEQUENCES:
    res = vui_delete_sequences(ctx, arg);
    break;

  case VUI_GENERATE_RANDOM:
    res = vui_generate_random(ctx, arg);
    break;

  case VUI_LOCK:
    res = vui_lock(ctx, arg);
    break;

  case VUI_POST_SEQUENCE:
    res = vui_post_sequence(ctx, arg);
    break;

  case VUI_RENAME_SEQUENCE:
    res = vui_rename_sequence(ctx, arg);
    break;

  case VUI_RESET_3D:
    res = vui_reset_3d(ctx);
    break;

  case VUI_SELECT_WEBCAM:
    res = vui_select_webcam(ctx, arg);
    break;

  case VUI_SELECTOR_CHANGE:
    res = vui_selector_change(ctx, arg);
    break;

  case VUI_SHORTCUT:
    res = vui_shortcut(ctx, arg);
    break;

  case VUI_TOGGLE:
    res = vui_toggle(ctx, arg);
    break;

  case VUI_USE_SEQUENCE:
    res = vui_use_sequence(ctx, arg);
    break;

  default:
    break;
  }

  return res;
}
