/*
 *  Copyright 1994-2022 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"


json_t *
vui_use_sequence(Context_t *ctx, const json_t *arg)
{
  json_t *res = NULL;

  if (NULL != arg) {
    json_t *id_j = json_object_get(arg, "id");

    if (json_is_integer(id_j)) {
      const uint64_t id = json_integer_value(id_j);
      Context_set_sequence(ctx, id);
      Alarm_init(ctx->a_random);
      res = json_pack("{so}", "sequence", Sequence_to_json(ctx, ctx->sm->cur, 1, 0, ctx->sm->cur->name));
    }
  }

  return res;
}
