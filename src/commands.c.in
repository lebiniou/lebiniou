# --------------------------------------------------------------------
# File format:
#
# Comments: begin with "#"
#
# 1. Section header:
# * Section name
# ** to end a section
#
# 2. Binding:
# ID MOD KEY CMD COMMENT
#
#  ID      = Binding identifier: id|-
#  MOD     = Keyboard modifier: -/A/C/S (none/Alt/Ctrl/Shift)
#  KEY     = SDL key
#  CMD     = command
#  COMMENT = comment
#
# ***IMPORTANT*** - Separator between items MUST be a single space
#
# --------------------------------------------------------------------

# --------------------------------------------------------------------
* Application
# --------------------------------------------------------------------
C1 AS r CMD_APP_RANDOMIZE_3D_ROTATIONS Random 3D rotations
C2 A b CMD_APP_NEXT_3D_BOUNDARY Next 3D boundary
C3 A r CMD_APP_TOGGLE_3D_ROTATIONS Toggle 3D rotations

C4 A c CMD_APP_DISPLAY_COLORMAP Display current colormap

C5 C f CMD_APP_SWITCH_FULLSCREEN Toggle full-screen on/off
C6 A m CMD_APP_SWITCH_CURSOR Show/hide mouse cursor

C9 S q CMD_APP_QUIT Quit
C10 S x CMD_APP_SAVE_QUIT Save the current sequence then quit

C11 S n CMD_APP_RANDOMIZE_SCREEN Fill current frame with random pixels
C12 - n CMD_APP_CLEAR_SCREEN Clear the current frame

C13 - ESCAPE CMD_APP_STOP_AUTO_MODES Turn off all auto changes
C14 - RETURN CMD_APP_TOGGLE_SELECTED_PLUGIN Toggle selected plugin on/off

C15 S LEFT CMD_APP_PREVIOUS_SEQUENCE Use previous sequence
C16 S RIGHT CMD_APP_NEXT_SEQUENCE Use next sequence

C17 S PRINTSCREEN CMD_APP_SCREENSHOT Take a screenshot

C18 C BACKSPACE CMD_APP_RANDOM_SCHEME Make a sequence from system schemes
C19 - BACKSPACE CMD_APP_RANDOM_SEQUENCE Select a random user sequence

C20 - m CMD_APP_NEXT_RANDOM_MODE Next random mode
C21 S m CMD_APP_PREVIOUS_RANDOM_MODE Previous random mode

C22 C t CMD_APP_TOGGLE_AUTO_COLORMAPS Auto colormaps on/off
C23 C i CMD_APP_TOGGLE_AUTO_IMAGES Auto images on/off

C25 - SPACE CMD_APP_SWITCH_BYPASS Bypass webcams on/off
C26 C SPACE CMD_APP_SET_WEBCAM_REFERENCE Set webcam reference image

# Obsolete ? OpenGL controls
# C27 C v BT_CONTEXT BC_SWITCH BA_BOUNDARY Switch 3d cube on/off
# C28 C c BT_CONTEXT BC_SWITCH BA_PULSE Toggle 3D world pulse on/off

C29 - TAB CMD_APP_NEXT_WEBCAM Select next webcam

C30 C l CMD_APP_LOCK_SELECTED_PLUGIN Lock selected plugin

C33 C TAB CMD_APP_FREEZE_INPUT Freeze input on/off

C34 S v CMD_APP_VOLUME_SCALE_UP Scale volume up
C35 A v CMD_APP_VOLUME_SCALE_DOWN Scale volume down

C36 CS LEFT CMD_APP_FIRST_SEQUENCE Use first sequence
C37 CS RIGHT CMD_APP_LAST_SEQUENCE Use last sequence

C38 S PAGEUP CMD_APP_DEC_3D_SCALE Increase 3D zoom
C39 S PAGEDOWN CMD_APP_INC_3D_SCALE Decrease 3D zoom
**

# --------------------------------------------------------------------
* Plugins
# --------------------------------------------------------------------
P1 - UP CMD_PLG_PREVIOUS Select previous plugin
P2 - DOWN CMD_PLG_NEXT Select next plugin

P3 - PAGEUP CMD_PLG_SCROLL_UP Scroll up in the plugins list
P4 - PAGEDOWN CMD_PLG_SCROLL_DOWN Scroll down in the plugins list

# - A r BT_PLUGINS BC_RELOAD BA_SELECTED Reload selected plugin .so
**

# --------------------------------------------------------------------
* Sequences
# --------------------------------------------------------------------
S1 S z CMD_SEQ_RESET Reset the current sequence
S2 S l CMD_SEQ_TOGGLE_LENS Toggle selected plugin as a lens on/off

S3 S UP CMD_SEQ_SELECT_PREVIOUS Select previous plugin in the sequence
S4 S DOWN CMD_SEQ_SELECT_NEXT Select next plugin in the sequence

S5 C UP CMD_SEQ_MOVE_UP Move selected plugin up in the sequence
S6 C DOWN CMD_SEQ_MOVE_DOWN Move selected plugin down in the sequence

S7 A y CMD_SEQ_LAYER_DEFAULT Select default layer mode for the current plugin
- C y CMD_SEQ_LAYER_PREVIOUS Select previous layer mode
S9 S y CMD_SEQ_LAYER_NEXT Select next layer mode

S10 C s CMD_SEQ_SAVE_FULL Save current sequence as new (full)
S11 C u CMD_SEQ_UPDATE_FULL Update current full sequence
S12 CS s CMD_SEQ_SAVE_BARE Save current sequence as new (bare)
S13 CS u CMD_SEQ_UPDATE_BARE Update current bare sequence

S14 A UP CMD_SEQ_PARAM_PREVIOUS Select previous plugin parameter
S15 A DOWN CMD_SEQ_PARAM_NEXT Select next plugin parameter

S16 A LEFT CMD_SEQ_PARAM_DEC Decrease plugin parameter value
S17 A RIGHT CMD_SEQ_PARAM_INC Increase plugin parameter value
S18 AS LEFT CMD_SEQ_PARAM_DEC_FAST Decrease plugin parameter value quickly
S19 AS RIGHT CMD_SEQ_PARAM_INC_FAST Increase plugin parameter value quickly
**

# --------------------------------------------------------------------
* Colormaps
# --------------------------------------------------------------------
CO1 - e CMD_COL_PREVIOUS Select previous colormap
CO2 - r CMD_COL_NEXT Select next colormap
CO3 - t CMD_COL_RANDOM Select random colormap
**

* Colormap shortcuts
C11 S 1 CMD_COL_USE_SHORTCUT_1 Use colormap in shortcut 1
C12 S 2 CMD_COL_USE_SHORTCUT_2 Use colormap in shortcut 2
C13 S 3 CMD_COL_USE_SHORTCUT_3 Use colormap in shortcut 3
C14 S 4 CMD_COL_USE_SHORTCUT_4 Use colormap in shortcut 4
C15 S 5 CMD_COL_USE_SHORTCUT_5 Use colormap in shortcut 5
C16 S 6 CMD_COL_USE_SHORTCUT_6 Use colormap in shortcut 6
C17 S 7 CMD_COL_USE_SHORTCUT_7 Use colormap in shortcut 7
C18 S 8 CMD_COL_USE_SHORTCUT_8 Use colormap in shortcut 8
C19 S 9 CMD_COL_USE_SHORTCUT_9 Use colormap in shortcut 9
C20 S 0 CMD_COL_USE_SHORTCUT_10 Use colormap in shortcut 10

C21 CS 1 CMD_COL_STORE_SHORTCUT_1 Assign current colormap to shortcut 1
C22 CS 2 CMD_COL_STORE_SHORTCUT_2 Assign current colormap to shortcut 2
C23 CS 3 CMD_COL_STORE_SHORTCUT_3 Assign current colormap to shortcut 3
C24 CS 4 CMD_COL_STORE_SHORTCUT_4 Assign current colormap to shortcut 4
C25 CS 5 CMD_COL_STORE_SHORTCUT_5 Assign current colormap to shortcut 5
C26 CS 6 CMD_COL_STORE_SHORTCUT_6 Assign current colormap to shortcut 6
C27 CS 7 CMD_COL_STORE_SHORTCUT_7 Assign current colormap to shortcut 7
C28 CS 8 CMD_COL_STORE_SHORTCUT_8 Assign current colormap to shortcut 8
C29 CS 9 CMD_COL_STORE_SHORTCUT_9 Assign current colormap to shortcut 9
C30 CS 0 CMD_COL_STORE_SHORTCUT_10 Assign current colormap to shortcut 10
**

# --------------------------------------------------------------------
* Images
# --------------------------------------------------------------------
IM1 - y CMD_IMG_PREVIOUS Select previous image
IM2 - u CMD_IMG_NEXT Select next image
IM3 - i CMD_IMG_RANDOM Select random image
**

* Image shortcuts
IM11 A 1 CMD_IMG_USE_SHORTCUT_1 Use image in shortcut 1
IM12 A 2 CMD_IMG_USE_SHORTCUT_2 Use image in shortcut 2
IM13 A 3 CMD_IMG_USE_SHORTCUT_3 Use image in shortcut 3
IM14 A 4 CMD_IMG_USE_SHORTCUT_4 Use image in shortcut 4
IM15 A 5 CMD_IMG_USE_SHORTCUT_5 Use image in shortcut 5
IM16 A 6 CMD_IMG_USE_SHORTCUT_6 Use image in shortcut 6
IM17 A 7 CMD_IMG_USE_SHORTCUT_7 Use image in shortcut 7
IM18 A 8 CMD_IMG_USE_SHORTCUT_8 Use image in shortcut 8
IM19 A 9 CMD_IMG_USE_SHORTCUT_9 Use image in shortcut 9
IM20 A 0 CMD_IMG_USE_SHORTCUT_10 Use image in shortcut 10

IM21 CA 1 CMD_IMG_STORE_SHORTCUT_1 Assign current image to shortcut 1
IM22 CA 2 CMD_IMG_STORE_SHORTCUT_2 Assign current image to shortcut 2
IM23 CA 3 CMD_IMG_STORE_SHORTCUT_3 Assign current image to shortcut 3
IM24 CA 4 CMD_IMG_STORE_SHORTCUT_4 Assign current image to shortcut 4
IM25 CA 5 CMD_IMG_STORE_SHORTCUT_5 Assign current image to shortcut 5
IM26 CA 6 CMD_IMG_STORE_SHORTCUT_6 Assign current image to shortcut 6
IM27 CA 7 CMD_IMG_STORE_SHORTCUT_7 Assign current image to shortcut 7
IM28 CA 8 CMD_IMG_STORE_SHORTCUT_8 Assign current image to shortcut 8
IM29 CA 9 CMD_IMG_STORE_SHORTCUT_9 Assign current image to shortcut 9
IM30 CA 0 CMD_IMG_STORE_SHORTCUT_10 Assign current image to shortcut 10
**

# --------------------------------------------------------------------
* Banks
# --------------------------------------------------------------------
CB1 CS F1 CMD_APP_CLEAR_BANK_1 Clear bank 1
CB2 CS F2 CMD_APP_CLEAR_BANK_2 Clear bank 2
CB3 CS F3 CMD_APP_CLEAR_BANK_3 Clear bank 3
CB4 CS F4 CMD_APP_CLEAR_BANK_4 Clear bank 4
CB5 CS F5 CMD_APP_CLEAR_BANK_5 Clear bank 5
CB6 CS F6 CMD_APP_CLEAR_BANK_6 Clear bank 6
CB7 CS F7 CMD_APP_CLEAR_BANK_7 Clear bank 7
CB8 CS F8 CMD_APP_CLEAR_BANK_8 Clear bank 8
CB9 CS F9 CMD_APP_CLEAR_BANK_9 Clear bank 9
CB10 CS F10 CMD_APP_CLEAR_BANK_10 Clear bank 10
CB11 CS F11 CMD_APP_CLEAR_BANK_11 Clear bank 11
CB12 CS F12 CMD_APP_CLEAR_BANK_12 Clear bank 12
CB13 CS F13 CMD_APP_CLEAR_BANK_13 Clear bank 13
CB14 CS F14 CMD_APP_CLEAR_BANK_14 Clear bank 14
CB15 CS F15 CMD_APP_CLEAR_BANK_15 Clear bank 15
CB16 CS F16 CMD_APP_CLEAR_BANK_16 Clear bank 16
CB17 CS F17 CMD_APP_CLEAR_BANK_17 Clear bank 17
CB18 CS F18 CMD_APP_CLEAR_BANK_18 Clear bank 18
CB19 CS F19 CMD_APP_CLEAR_BANK_19 Clear bank 19
CB20 CS F20 CMD_APP_CLEAR_BANK_20 Clear bank 20
CB21 CS F21 CMD_APP_CLEAR_BANK_21 Clear bank 21
CB22 CS F22 CMD_APP_CLEAR_BANK_22 Clear bank 22
CB23 CS F23 CMD_APP_CLEAR_BANK_23 Clear bank 23
CB24 CS F24 CMD_APP_CLEAR_BANK_24 Clear bank 24

SB1 S F1 CMD_APP_STORE_BANK_1 Assign current sequence to bank 1
SB2 S F2 CMD_APP_STORE_BANK_2 Assign current sequence to bank 2
SB3 S F3 CMD_APP_STORE_BANK_3 Assign current sequence to bank 3
SB4 S F4 CMD_APP_STORE_BANK_4 Assign current sequence to bank 4
SB5 S F5 CMD_APP_STORE_BANK_5 Assign current sequence to bank 5
SB6 S F6 CMD_APP_STORE_BANK_6 Assign current sequence to bank 6
SB7 S F7 CMD_APP_STORE_BANK_7 Assign current sequence to bank 7
SB8 S F8 CMD_APP_STORE_BANK_8 Assign current sequence to bank 8
SB9 S F9 CMD_APP_STORE_BANK_9 Assign current sequence to bank 9
SB10 S F10 CMD_APP_STORE_BANK_10 Assign current sequence to bank 10
SB11 S F11 CMD_APP_STORE_BANK_11 Assign current sequence to bank 11
SB12 S F12 CMD_APP_STORE_BANK_12 Assign current sequence to bank 12
SB13 - - CMD_APP_STORE_BANK_13 Assign current sequence to bank 13
SB14 - - CMD_APP_STORE_BANK_14 Assign current sequence to bank 14
SB15 - - CMD_APP_STORE_BANK_15 Assign current sequence to bank 15
SB16 - - CMD_APP_STORE_BANK_16 Assign current sequence to bank 16
SB17 - - CMD_APP_STORE_BANK_17 Assign current sequence to bank 17
SB18 - - CMD_APP_STORE_BANK_18 Assign current sequence to bank 18
SB19 - - CMD_APP_STORE_BANK_19 Assign current sequence to bank 19
SB20 - - CMD_APP_STORE_BANK_20 Assign current sequence to bank 20
SB21 - - CMD_APP_STORE_BANK_21 Assign current sequence to bank 21
SB22 - - CMD_APP_STORE_BANK_22 Assign current sequence to bank 22
SB23 - - CMD_APP_STORE_BANK_23 Assign current sequence to bank 23
SB24 - - CMD_APP_STORE_BANK_24 Assign current sequence to bank 24

UB1 AS F1 CMD_APP_USE_BANK_1 Use sequence in bank 1
UB2 AS F2 CMD_APP_USE_BANK_2 Use sequence in bank 2
UB3 AS F3 CMD_APP_USE_BANK_3 Use sequence in bank 3
UB4 AS F4 CMD_APP_USE_BANK_4 Use sequence in bank 4
UB5 AS F5 CMD_APP_USE_BANK_5 Use sequence in bank 5
UB6 AS F6 CMD_APP_USE_BANK_6 Use sequence in bank 6
UB7 AS F7 CMD_APP_USE_BANK_7 Use sequence in bank 7
UB8 AS F8 CMD_APP_USE_BANK_8 Use sequence in bank 8
UB9 AS F9 CMD_APP_USE_BANK_9 Use sequence in bank 9
UB10 AS F10 CMD_APP_USE_BANK_10 Use sequence in bank 10
UB11 AS F11 CMD_APP_USE_BANK_11 Use sequence in bank 11
UB12 AS F12 CMD_APP_USE_BANK_12 Use sequence in bank 12
UB13 AS F13 CMD_APP_USE_BANK_13 Use sequence in bank 13
UB14 AS F14 CMD_APP_USE_BANK_14 Use sequence in bank 14
UB15 AS F15 CMD_APP_USE_BANK_15 Use sequence in bank 15
UB16 AS F16 CMD_APP_USE_BANK_16 Use sequence in bank 16
UB17 AS F17 CMD_APP_USE_BANK_17 Use sequence in bank 17
UB18 AS F18 CMD_APP_USE_BANK_18 Use sequence in bank 18
UB19 AS F19 CMD_APP_USE_BANK_19 Use sequence in bank 19
UB20 AS F20 CMD_APP_USE_BANK_20 Use sequence in bank 20
UB21 AS F21 CMD_APP_USE_BANK_21 Use sequence in bank 21
UB22 AS F22 CMD_APP_USE_BANK_22 Use sequence in bank 22
UB23 AS F23 CMD_APP_USE_BANK_23 Use sequence in bank 23
UB24 AS F24 CMD_APP_USE_BANK_24 Use sequence in bank 24

B1 CAS F1 CMD_APP_USE_BANK_SET_1 Use bank set 1
B2 CAS F2 CMD_APP_USE_BANK_SET_2 Use bank set 2
B3 CAS F3 CMD_APP_USE_BANK_SET_3 Use bank set 3
B4 CAS F4 CMD_APP_USE_BANK_SET_4 Use bank set 4
B5 CAS F5 CMD_APP_USE_BANK_SET_5 Use bank set 5
B6 CAS F6 CMD_APP_USE_BANK_SET_6 Use bank set 6
B7 CAS F7 CMD_APP_USE_BANK_SET_7 Use bank set 7
B8 CAS F8 CMD_APP_USE_BANK_SET_8 Use bank set 8
B9 CAS F9 CMD_APP_USE_BANK_SET_9 Use bank set 9
B10 CAS F10 CMD_APP_USE_BANK_SET_10 Use bank set 10
B11 CAS F11 CMD_APP_USE_BANK_SET_11 Use bank set 11
B12 CAS F12 CMD_APP_USE_BANK_SET_12 Use bank set 12
B13 CAS F13 CMD_APP_USE_BANK_SET_13 Use bank set 13
B14 CAS F14 CMD_APP_USE_BANK_SET_14 Use bank set 14
B15 CAS F15 CMD_APP_USE_BANK_SET_15 Use bank set 15
B16 CAS F16 CMD_APP_USE_BANK_SET_16 Use bank set 16
B17 CAS F17 CMD_APP_USE_BANK_SET_17 Use bank set 17
B18 CAS F18 CMD_APP_USE_BANK_SET_18 Use bank set 18
B19 CAS F19 CMD_APP_USE_BANK_SET_19 Use bank set 19
B20 CAS F20 CMD_APP_USE_BANK_SET_20 Use bank set 20
B21 CAS F21 CMD_APP_USE_BANK_SET_21 Use bank set 21
B22 CAS F22 CMD_APP_USE_BANK_SET_22 Use bank set 22
B23 CAS F23 CMD_APP_USE_BANK_SET_23 Use bank set 23
B24 CAS F24 CMD_APP_USE_BANK_SET_24 Use bank set 24
**

# --------------------------------------------------------------------
# Web UI commands
# --------------------------------------------------------------------
0 - - UI_CMD_CONNECT Connect to the JQuery interface

0 - - UI_CMD_APP_SELECT_PLUGIN Select plugin
0 - - UI_CMD_APP_SET_3D_ROTATION_AMOUNT Set rotation amount
0 - - UI_CMD_APP_SET_3D_ROTATION_FACTOR Set rotation factor
0 - - UI_CMD_APP_SET_AUTO_MODE Set auto change mode
0 - - UI_CMD_APP_SET_BANDPASS Set bandpass range
0 - - UI_CMD_APP_SET_DELAY Set timer delays
0 - - UI_CMD_APP_SET_FADE_DELAY Set fade delay
0 - - UI_CMD_APP_SET_MAX_FPS Set maximum frames per second
0 - - UI_CMD_APP_SET_VOLUME_SCALE Set volume scale
0 - - UI_CMD_APP_TOGGLE_RANDOM_SCHEMES Random schemes on/off
0 - - UI_CMD_APP_TOGGLE_RANDOM_SEQUENCES Random sequences on/off

0 - - UI_CMD_COL_PREVIOUS_N Select previous colormap (fast)
0 - - UI_CMD_COL_NEXT_N Select previous colormap (fast)
0 - - UI_CMD_IMG_PREVIOUS_N Select previous image (fast)
0 - - UI_CMD_IMG_NEXT_N Select previous image (fast)

0 - - UI_CMD_SEQ_RENAME Rename a sequence
0 - - UI_CMD_SEQ_REORDER Reorder a sequence
0 - - UI_CMD_SEQ_SET_LAYER_MODE Set a layer mode
0 - - UI_CMD_SEQ_SET_PARAM_CHECKBOX_VALUE Set parameter value from a checkbox
0 - - UI_CMD_SEQ_SET_PARAM_PLAYLIST_VALUE Set parameter value from a file input
0 - - UI_CMD_SEQ_SET_PARAM_SLIDER_VALUE Set parameter value from a slider
0 - - UI_CMD_SEQ_SET_PARAM_SELECT_VALUE Set parameter value from a select

0 - - UI_CMD_TRACKBALL_ON_DRAG_START Drag start event
0 - - UI_CMD_TRACKBALL_ON_DRAG_MOVE Drag move event
0 - - UI_CMD_TRACKBALL_ON_MOUSE_WHEEL Mouse wheel event

0 - - UI_CMD_OUTPUT Output plugin command

0 - - UI_CMD_NOOP No operation

0 - - UI_CMD_BANK Bank command (store/use/clear)

0 - - UI_CMD_SELECT_ITEM Select a colormap/image

# --------------------------------------------------------------------
# Vue UI commands
# --------------------------------------------------------------------
0 - - VUI_CONNECT Connect to the Vue interface
0 - - VUI_DELETE_SEQUENCES Delete sequences
0 - - VUI_GENERATE_RANDOM Generate a random sequence
0 - - VUI_LOCK Lock a colormap or image
0 - - VUI_POST_SEQUENCE Upload a new sequence
0 - - VUI_RENAME_SEQUENCE Rename a sequence
0 - - VUI_RESET_3D Reset 3D parameters
0 - - VUI_SELECT_WEBCAM Select a webcam
0 - - VUI_SELECTOR_CHANGE Selector previous/next/random commands
0 - - VUI_SHORTCUT Shortcuts-related commands
0 - - VUI_USE_SEQUENCE Use a sequence
0 - - VUI_TOGGLE Toggle auto colormaps/images/webcams
